# Devinette d'un nombre
 Ce projet consiste à Créer un mini-jeu de devinette en Python en utilisant Git. Le programme choisit un nombre aléatoire entre 1 et 100. L'utilisateur doit deviner le nombre en moins de 10 essais. L’objectif est d’apprendre les bases de la gestion de versions et se familiariser avec un workflow Git simple. Veuillez lire attentivement les consignes avant d’y répondre. N’oubliez pas de rendre votre travail public sur Gitlab.

## Questions de TP"Devinette d'un nombre"
1- Créer un nouveau projet sur Gitlab nommé « Devinette d'un nombre » et le cloner sur votre ordinateur.

![Question1.jpg](Question1.jpg)

2- Créer dans le dossier cloné un fichier « main.py » et écrire le programme du jeu.

![Question2.jpg](Question2.jpg)

![Question3.jpg](Question3.jpg)

3- Propagez les modifications sur le dépôt distant avec un message de commit clair et précis.

![Question2.jpg](Question2.jpg)

4- Créez une nouvelle branche « dev ».

![Question4.jpg](Question4.jpg)

5- Développer dans cette branche la possibilité que le programme affiche des indices pour aider l'utilisateur (plus grand, plus petit) et qu’il affiche à la fin après combien d’essais.

![Question5.jpg](Question5.jpg)

6- Sauvegarder les modifications sur le dépôt distant.

![Question6.jpg](Question6.jpg)

7- Procéder à la fusion de la banche « dev » avec la branche principale.

![Question7.jpg](Question7.jpg)

![Question8.jpg](Question8.jpg)

8- Créer un fichier README.md qui contient les questions de ce TP (la question 7 exclue) et pour chacune d’elle, une capture d’écran montrant comment vous y avez répondu. Propager ce fichier sur le dépôt distant.(Voir précédement)

